
import process from "process"
import os from "os"
import dns from "dns"
import {getDiskInfoSync} from "node-disk-info"

const getArch = () => {
    const arch = os.arch()
    console.log(`Your arch is ${arch}`)
}

const getCpus = () => {
    const cpus = os.cpus();
    const numOfCores = cpus.length;
    console.log(`Your cpu is ${cpus[0].model} \nWith ${numOfCores} cores \nat ${cpus[0].speed / 1000} GHz`)
}

const getRam = () => {
    const totalMem = os.totalmem();
    const usedMem = os.freemem();
    console.log(`Your computer has total of ${(totalMem / 1024  / 1024 / 1024).toFixed(2)} GB usable ram`);
    console.log(`Your computer is using ${(usedMem / 1024  / 1024 / 1024).toFixed(2)} GB ram`);
    console.log(`Your computer has ${((totalMem - usedMem) / 1024  / 1024 / 1024).toFixed(2)} GB free ram`);

}

const getDiskSpace = () => {
    const disks = getDiskInfoSync()
    disks.forEach(d => {
        console.log(`Disk name: ${d.mounted} \nUsed space: ${(d.used / 1024 / 1024 / 1024).toFixed(2)} GB \nAvailable capacity ${(d.available / 1024 / 1024 / 1024).toFixed(2)} GB`)
    })
}

const getHostname = () => {
    const hostname = os.hostname();
    console.log(`Your hostname is ${hostname}`)

}

const getIP = () => {
    const ip = dns.lookup(os.hostname(), (_err, add ,_) => {
        console.log(`Your ip is ${add}`)
    });
    
}

const main = () => {
    let action = process.argv[2];
    switch (action) {
        case "-arch":
            getArch();
            break;
        case "-cpu":
            getCpus()
            break;
        case "-ram":
            getRam()
            break;
        case "-hdd":
            getDiskSpace()
            break;
        case "-hostname":
            getHostname()
            break;
        case "-ip":
            getIP()
            break;
        default: 
            console.log("Invalid arg")
        
    }

}

main()